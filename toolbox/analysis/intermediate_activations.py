import PIL
import torch
import torchvision
from torchvision import models, transforms

import numpy as np
import matplotlib.pyplot as plt


def imshow_v2(tensor, labels, nclass):
    """

    :param tensor: 
    :param labels: 
    :param nclass: 

    """
    plt.figure(figsize=(10,6))
    for i, img in enumerate(tensor):
        plt.subplot(4,4,i+1)
        plt.axis('off')
        # print labels
        plt.title(nclass[labels[i]])
        img = img / 2 + 0.5     # unnormalize
        npimg = img.numpy()
        plt.imshow(np.transpose(npimg, (1, 2, 0)))
    plt.tight_layout()
    plt.show()

if __name__ == '__main__':
    NUM_CLASSES = 10
    device = torch.device('cpu')

    # Define categories of dataset and point to direction to generate data flow to train.
    classes = ['airplane', 'bird', 'car', 'cat', 'deer', 'dog', 'horse', 'monkey', 'ship', 'truck']
    samples = torchvision.datasets.ImageFolder('../dataset/samples',
                                               transform=transforms.Compose([
                                                   transforms.Resize(100),
                                                   transforms.RandomCrop(96, 4),
                                                   transforms.RandomRotation((-180,180), resample=PIL.Image.BILINEAR),
                                                   transforms.ToTensor()
                                               ]))
    sample_loader = torch.utils.data.DataLoader(samples, batch_size=1,
                                                shuffle=False, num_workers=2)

    # Load a existed net into CPU device to visualize
    # Loading net shouldn't to require NetTrainer module
    checkpoint = torch.load('../classifier/resnet18-STL10-checkpoint.pth', map_location=device)
    net = models.resnet18(num_classes=NUM_CLASSES)
    net.load_state_dict(checkpoint['model_state_dict'])

    net.eval()

    dataiter = iter(sample_loader)
    data, _ = dataiter.next()
    # plt.imshow(np.transpose(data[0].numpy(), (1, 2, 0)))
    # plt.show()
    # imshow_v2(data, net(data).argmax(dim=1), classes)

    for name, layer in net._modules.items():
        pass
