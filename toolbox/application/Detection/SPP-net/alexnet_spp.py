import time

import matplotlib.pyplot as plt
import numpy as np
import PIL
import torch
import torch.nn as nn
import torch.optim as optim
import torchvision
import torchvision.transforms as transforms


# Spatial Pyramid Pooling block
# https://arxiv.org/abs/1406.4729
class SPPooling(nn.Module):
    __constants__ = ['output_size', 'pooling_type']

    def __init__(self, output_size, pooling_type='max'):
        '''
        output_size: the height and width after spp layer
        pooling_type: the type of pooling 
        '''
        super(SPPooling, self).__init__()
        self.output_size = output_size
        self.pooling_type = pooling_type

    def extra_repr(self):
        return 'output_size={output_size}, pooling_type={pooling_type}'.format(
            **self.__dict__)

    def forward(self, x):
        N, C, H, W = x.size()
        ## the size of pooling window
        sizeX = int(np.ceil(H / self.output_size))
        ## the strides of pooling
        stride = int(np.floor(H / self.output_size))
        if self.pooling_type == 'max':
            self.spp = nn.MaxPool2d(kernel_size=sizeX, stride=stride)
        else:
            self.spp = nn.AdaptiveAvgPool2d(kernel_size=sizeX, stride=stride)
        x = self.spp(x)

        return x


normalize10 = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                   std=[0.229, 0.224, 0.225])
normalize100 = transforms.Normalize(mean=[0.5071, 0.4865, 0.4409],
                                    std=[0.2673, 0.2564, 0.2762])

transform = transforms.Compose([
    transforms.Resize((64, 64)),
    # transforms.ColorJitter(0.1),
    #transforms.RandomRotation((-180, 180), resample=PIL.Image.BILINEAR),
    # transforms.RandomResizedCrop((32,32)),
    # transforms.RandomHorizontalFlip(),
    # transforms.RandomVerticalFlip(),
    #transforms.RandomCrop(32, 4),
    #transforms.CenterCrop(32),
    transforms.ToTensor(),
    normalize100
])

trainset = torchvision.datasets.CIFAR100(root='./data',
                                         train=True,
                                         download=True,
                                         transform=transform)
trainloader = torch.utils.data.DataLoader(trainset,
                                          batch_size=32,
                                          shuffle=True,
                                          num_workers=2)

testset = torchvision.datasets.CIFAR100(root='./data',
                                        train=False,
                                        download=True,
                                        transform=transform)
testloader = torch.utils.data.DataLoader(testset,
                                         batch_size=32,
                                         shuffle=False,
                                         num_workers=2)

classes10 = ('plane', 'car', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse',
             'ship', 'truck')

classes100 = ('apple', 'aquarium_fish', 'baby', 'bear', 'beaver', 'bed', 'bee',
              'beetle', 'bicycle', 'bottle', 'bowl', 'boy', 'bridge', 'bus',
              'butterfly', 'camel', 'can', 'castle', 'caterpillar', 'cattle',
              'chair', 'chimpanzee', 'clock', 'cloud', 'cockroach', 'couch',
              'crab', 'crocodile', 'cup', 'dinosaur', 'dolphin', 'elephant',
              'flatfish', 'forest', 'fox', 'girl', 'hamster', 'house',
              'kangaroo', 'keyboard', 'lamp', 'lawn_mower', 'leopard', 'lion',
              'lizard', 'lobster', 'man', 'maple_tree', 'motorcycle',
              'mountain', 'mouse', 'mushroom', 'oak_tree', 'orange', 'orchid',
              'otter', 'palm_tree', 'pear', 'pickup_truck', 'pine_tree',
              'plain', 'plate', 'poppy', 'porcupine', 'possum', 'rabbit',
              'raccoon', 'ray', 'road', 'rocket', 'rose', 'sea', 'seal',
              'shark', 'shrew', 'skunk', 'skyscraper', 'snail', 'snake',
              'spider', 'squirrel', 'streetcar', 'sunflower', 'sweet_pepper',
              'table', 'tank', 'telephone', 'television', 'tiger', 'tractor',
              'train', 'trout', 'tulip', 'turtle', 'wardrobe', 'whale',
              'willow_tree', 'wolf', 'woman', 'worm')


def imshow_v2(tensor, label, nclass):
    plt.figure(figsize=(14, 12))
    for i, img in enumerate(tensor):
        plt.subplot(4, 8, i + 1)
        plt.axis('off')
        # print labels
        plt.title(classes100[labels[i]])
        img = img / 2 + 0.5  # unnormalize
        npimg = img.numpy()
        plt.imshow(np.transpose(npimg, (1, 2, 0)))
    plt.tight_layout()
    plt.show()


# get some random training images
dataiter = iter(trainloader)
images, labels = dataiter.next()

# show images
imshow_v2(images, labels, len(classes100))

net = torchvision.models.alexnet()
net.avgpool = SPPooling(6)

# GPU
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
net.to(device)

criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)

st_tik = time.perf_counter()
for epoch in range(50):  # loop over the dataset multiple times

    running_loss = 0.0
    for i, data in enumerate(trainloader, 0):
        # get the inputs; data is a list of [inputs, labels]
        #inputs, labels = data
        tik = time.perf_counter()
        inputs, labels = data[0].to(device), data[1].to(device)

        # zero the parameter gradients
        optimizer.zero_grad()

        # forward + backward + optimize
        outputs = net(inputs)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()

        # print statistics
        running_loss += loss.item()
        if i % 200 == 199:  # print every 2000 mini-batches
            print('epoch #%d, mini-batch #%d , cost: %.3f, loss: %.3f' %
                  (epoch + 1, i + 1,
                   (200 * (time.perf_counter() - tik)), running_loss / 200))
            running_loss = 0.0

print(f'Finished Training in ~{(time.perf_counter() - st_tik)/60}m')
