(import torch)
(import [torch [nn optim]])
(import [torchvision [models datasets transforms]])
(import [utils.utils [accuracy AverageMeter]])

(setv __all__ ["create_train_fn" "create_valid_fn"])

(defmacro create-torch-progress! [fname watcher-module loss-module measure-module log-module]
  `(setv ~fname (fn [net criterion optimizer dataloader &optional]
                  ~watcher-module
                  (print "train net")
                  ;; control codegen
                  (cond [(= ~fname 'train) (.train net True)]
                        [(= ~fname 'test) (.train net False)])
                  (for [[it [inputs target]] (enumerate dataloader)]
                    ;; zero the optimizer
                    (.zero_grad optimizer)
                    ;; compute loss module
                    ~loss-module
                    ;; measure
                    ~measure-module
                    ;; backward, compute gradient and do optimizer
                    (.backward loss)
                    (.step optimizer)
                    ~log-module))))

;; expand macro
(defn create-train-fn [&optional
                       watcher-module
                       loss-module
                       measure-module
                       log-module]
  (create-torch-progress! train
                          ;; watcher-module
                          (setv acc1 (AverageMeter "Acc@1")
                                losses (AverageMeter "Loss"))
                          ;; loss-module
                          (setv output (net inputs)
                                loss (criterion output target))
                          ;; measure-module
                          (do
                            (setv acc_k (accuracy output target :topk (tuple [1])))
                            (.update losses (.item loss) (.size inputs 0))
                            (.update acc1 (.item (get acc_k 0)) (.size inputs 0)))
                          ;; log-module
                          (print losses.name losses.avg acc1.name acc1.avg))
  (return train))

(defn create-valid-fn [&optional
                       watcher-module
                       loss-module
                       measure-module
                       log-module]
  (create-torch-progress! valid
                          ;; watcher-module
                          (setv acc1 (AverageMeter "Acc@1")
                                losses (AverageMeter "Loss"))
                          ;; loss-module
                          (setv output (net inputs)
                                loss (criterion output target))
                          ;; measure-module
                          (do
                            (setv acc_k (accuracy output target :topk (tuple [1])))
                            (.update losses (.item loss) (.size inputs 0))
                            (.update acc1 (.item (get acc_k 0)) (.size inputs 0)))
                          ;; log-module
                          (print losses.name losses.avg acc1.name acc1.avg)))
;; define dataset
;; (setv transform (transforms.Compose (list [(transforms.ToTensor)]))
;;       dataset (datasets.CIFAR10 :root "~/.cache/torch/data"
;;                                 :train True
;;                                 :transform transform
;;                                 :download True)
;;       dataloader (torch.utils.data.DataLoader dataset
;;                                               :batch_size 64
;;                                               :shuffle True
;;                                               :num_workers 1))

;; (setv net (models.resnet18 :num_classes 10)
;;       criterion (nn.CrossEntropyLoss)
;;       optimizer (optim.Adam (.parameters net)))

;; (for [i (range 10)]
;;   (train net criterion optimizer dataloader)
;;   (test net criterion optimizer dataloader))
