Playground
==========

## Modules
1. trainer
2. benchmark
3. analysis
   - [x] intermediate activations
   - [x] convnet filters
   - [x] heatmaps of class activations*
4. transfer

## Deep Learning - TODO-List

### Computer Vision
1. Traditional computer vision method
   - [ ] Image Pyramids (WIP)
2. Image classifier
   - [ ] baseline benchmarks
3. Object detection
   - [x] R-CNN*, no bounding box regression and no hard negative mining
   - [ ] Fast R-CNN
   - [x] Faster R-CNN*
   - [x] Mask R-CNN*
   - [ ] SSD
   - [x] YOLO
   - [ ] YOLACT
   - [x] UNet ([mateuszbuda/brain-segmentation-pytorch](https://github.com/mateuszbuda/brain-segmentation-pytorch))
4. Layers
   - [x] SPPooling Layer
5. Preprocessing Metrics
   - [ ] AlexNet
6. Transfer Learning
   - [ ] ResNet and its variant

## Reinforcement Learning
Nothing here.
